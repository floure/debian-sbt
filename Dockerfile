FROM debian:buster
MAINTAINER Timothée Floure <timothee.floure@epfl.ch>

# Update local package index
RUN apt-get update

# Install OpenJDK 11.
RUN apt-get install -y openjdk-11-jdk

# Install upstream's Debian package for SBT v1.3.0.
RUN apt-get install -y wget
RUN wget https://dl.bintray.com/sbt/debian/sbt-1.3.0.deb
RUN dpkg -i sbt-1.3.0.deb
